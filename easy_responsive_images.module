<?php

/**
 * @file
 * DrImage core functions.
 */

use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function easy_responsive_images_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'easy_responsive_images.image':
      return '<p>' . t('For a full description of the module, visit the project page: https://drupal.org/project/drimage') . '</p>';
  }
}

/**
 * Implements hook_theme().
 */
function easy_responsive_images_theme() {
  return [
    'easy_responsive_images_formatter' => [
      'variables' => [
        'item' => NULL,
        'item_attributes' => NULL,
        'image_attributes' => [],
        'alt' => NULL,
        'width' => NULL,
        'height' => NULL,
        'loading_method' => NULL,
        'decoding_method' => NULL,
        'fetch_priority' => NULL,
        'data' => NULL,
        'src' => NULL,
        'srcset' => NULL,
        ]
        ],
// distinct srcset choosen image styles, estimated viewport, desired dimensions (dimensions), with fallback images_style
    'easy_responsive_image' => [
      'variables' => [
        'item' => NULL,
        'item_attributes' => NULL,
        'uri' => NULL,
        'default_image_style_id' => NULL,
        'possible_image_styles_ids' => NULL,
        'srcset' => NULL,
        'preferred_width' => NULL,
        'preferred_height' => NULL,
        'url' => NULL
      ],
    ],
  ];
}

/**
 * Prepares variables for easy_responsive_image template.
 *
 * Default template: easy_responsive_image.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - foo: Foo variable description.
 */
function template_preprocess_easy_responsive_image(array &$variables) {
//as image style formatter
  $style = ImageStyle::load($variables['default_image_style_id']);
  // Start with a tiny size as it will switch it later, using ResizeObserver JS.
  $variables['item_attributes']['sizes'] = "10vw"; 
  // Sizes will be what makes it to choose the adjusted container for this image
  // telling the browser which space it will spend.
  $variables['image'] = [
    '#theme' => 'image',
    '#width' => $variables['preferred_width'],
    '#height' => $variables['preferred_height'],
    '#attributes' => $variables['item_attributes'],
    '#srcset' => $variables['srcset'],
    '#style_name' => $variables['default_image_style_id'],
  ];

  // If the current image toolkit supports this file type, prepare the URI for
  // the derivative image. If not, just use the original image resized to the
  // dimensions specified by the style.
  if ($style->supportsUri($variables['uri'])) {
    $variables['image']['#uri'] = $style->buildUrl($variables['uri']);
  }
  else {
    $variables['image']['#uri'] = $variables['uri'];
    // Don't render the image by default, but allow other preprocess functions
    // to override that if they need to.
    $variables['image']['#access'] = FALSE;

    // Inform the site builders why their image didn't work.
    \Drupal::logger('image')->warning('Could not apply @style image style to @uri because the style does not support it.', [
      '@style' => $style->label(),
      '@uri' => $variables['image_uri'],
    ]);
  }

  $item = $variables['item'];
  //obtain some properties from media entity
  if (!is_null($item->title) && mb_strlen($item->title) != 0) {
    $variables['image']['#title'] = $item->title;
  }
  if (!is_null($item->alt) && mb_strlen($item->alt) != 0) {
    $variables['image']['#alt'] = $item->alt;
  }
 }
