<?php

namespace Drupal\easy_responsive_images\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\image\Entity\ImageStyle;

/**
 * Plugin implementation of the 'Easy responsive image' formatter.
 *
 * @FieldFormatter(
 *   id = "easy_responsive_images_easy_responsive_image",
 *   label = @Translation("Easy responsive image"),
 *   field_types = {
 *     "image"
 *   },
 *   quickedit = {
 *     "editor" = "image"
 *   }
 * )
 */
class EasyResponsiveImageFormatter extends ImageFormatter {

  /**
   * The image style entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a ResponsiveImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityStorageInterface $image_style_storage, AccountInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $current_user, $image_style_storage);
  }

   */
  /**
   * {@inheritdoc}
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('link_generator'),
      $container->get('current_user')
    );
  }
   */

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'srcset_responsive_image_styles' => [],
      'preferred_height' => '',
      'preferred_width' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $responsive_image_options = [];
    $image_styles_ids = $this->imageStyleStorage->getQuery()
      ->condition('name', 'responsive_', 'STARTS_WITH')
      ->execute();
    $responsive_image_styles = $this->imageStyleStorage->loadMultiple($image_styles_ids);
    uasort($responsive_image_styles, '\Drupal\Core\Config\Entity\ConfigEntityBase::sort');
    if ($responsive_image_styles && !empty($responsive_image_styles)) {
      foreach ($responsive_image_styles as $machine_name => $responsive_image_style) {
        //if ($responsive_image_style->hasImageStyleMappings()) {
          $responsive_image_options[$machine_name] = $responsive_image_style->label();
        //}
      }
    }

    $elements["image_style"]['#title'] = t('Default responsive image style');
    $elements["image_style"]['#options'] = $responsive_image_options;
    $elements['image_link']['#weight'] = 1;

    $elements['srcset_responsive_image_styles'] = [
      '#title' => t('Possible image styles'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('srcset_responsive_image_styles') ?: NULL,
      '#required' => TRUE,
      '#options' => $responsive_image_options,
      '#multiple' => TRUE,
      '#description' => $elements["image_style"]["#description"],
        #'#markup' => $this->linkGenerator->generate($this->t('Configure Image Styles'), new Url('entity.image_style.collection')),
    ];
    $elements['preferred_width'] = [
      
    ];
    $elements['preferred_width'] = array(
      '#title' => $this->t('Preferrer width'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('preferred_width') ? : NULL,
      '#size' => 8,
      '#maxlength' => 8,
      '#placeholder' => $this->t('width'),
      '#field_suffix' => ' × ',
    );
    $elements['preferred_height'] = array(
      '#title' => $this->t('Preferred height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('preferred_height') ? : NULL,
      '#size' => 8,
      '#maxlength' => 8,
      '#placeholder' => $this->t('height'),
    );
    
     return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary(); 

    $summary[] = $this->t('Possible image styles count: @count', ['@count' => $this->getSetting('srcset_responsive_image_styles')? count($this->getSetting('srcset_responsive_image_styles')): $this->t('None')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    $url = NULL;
    $image_link_setting = $this->getSetting('image_link');
    // Check if the formatter involves a link.
    if ($image_link_setting == 'content') {
      $entity = $items->getEntity();
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    elseif ($image_link_setting == 'file') {
      $link_file = TRUE;
    }

    $default_image_style_setting = $this->getSetting('image_style');

    // Collect cache tags to be added for each item in the field.
    $default_image_style = $this->imageStyleStorage->load($default_image_style_setting);
    $image_styles_to_load = [];
    $cache_tags = [];
    if ($default_image_style) {
      $cache_tags = Cache::mergeTags($cache_tags, $default_image_style->getCacheTags());
      $image_styles_to_load = $this->getSetting("srcset_responsive_image_styles"); //$responsive_image_style->getImageStyleIds();
    }

    $srcset_image_styles = $this->imageStyleStorage->loadMultiple($image_styles_to_load);
    $srcset_image_styles_ids = [];
    foreach ($srcset_image_styles as $srcset_image_style) {
      $cache_tags = Cache::mergeTags($cache_tags, $srcset_image_style->getCacheTags());
      $srcset_image_styles_ids[] = $srcset_image_style->id();
    }

    foreach ($files as $delta => $file) {
      assert($file instanceof FileInterface);
      // Link the <picture> element to the original file.
      if (isset($link_file)) {
        $url = $file->createFileUrl();
      }
      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $image_uri = $file->getFileUri();

      $srcset = [];
      foreach ($srcset_image_styles as $srcset_image_style) {
        $effects_configuration = $srcset_image_style->getEffects()->getConfiguration();
        $last_effect_config = end($effects_configuration);
        if ($last_effect_config){
          $srcset_item = $last_effect_config['data'];
          $srcset_item['width'] .= "w";
          $srcset_item['uri'] = $srcset_image_style->buildUrl($image_uri);
          $srcset[] = $srcset_item;
        }
      }

      $elements[$delta] = [
        '#theme' => 'easy_responsive_image',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#default_image_style_id' => $default_image_style ? $default_image_style->id() : '',
        '#possible_image_styles_ids' => $srcset_image_styles_ids,
        '#srcset' => $srcset,
        '#uri' => $image_uri,
        '#preferred_width' => $this->getSetting('preferred_width'),
        '#preferred_height' => $this->getSetting('preferred_height'),
        '#url' => $url,
        '#cache' => [
          'tags' => $cache_tags,
        ],
      ];
    }
    return $elements;
  }


  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $srcset_styles_id =  $this->getSetting('srcset_responsive_image_styles');
    foreach ($srcset_styles_id as $srcset_style_id){
      if ($srcset_style_id && $srcset_style = ImageStyle::load($srcset_style_id)) {
        // If this formatter uses a valid image style for this srcset item, add
        // the image style configuration entity as dependency of this formatter.
        $dependencies[$srcset_style->getConfigDependencyKey()][] = $srcset_style->getConfigDependencyName();
      }
    }
    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies) {
    $changed = parent::onDependencyRemoval($dependencies);
    $srcset_styles_id =  $this->getSetting('srcset_responsive_image_styles');
    $replacement_srcset = $srcset_styles_id;
    foreach ($srcset_styles_id as $key => $srcset_style_id){
      if ($srcset_style_id && $srcset_style = ImageStyle::load($srcset_style_id)) {
        if (!empty($dependencies[$srcset_style->getConfigDependencyKey()][$srcset_style->getConfigDependencyName()])) {
          $replacement_id = $this->imageStyleStorage->getReplacementId($srcset_styles_id);
          // If a valid replacement has been provided in the storage, replace
          // the image style with the replacement and signal that the formatter
          // plugin settings were updated.
          if ($replacement_id && ImageStyle::load($replacement_id)) {
            $replacement_srcset[$srcset_key] = $replacement_id;
            $changed = TRUE;
          } 
        }
      }
    }
    if ($changed) {
      $this->setSetting('srcset_responsive_image_styles', $replacement_srcset);
    }
    return $changed;
  }


}
