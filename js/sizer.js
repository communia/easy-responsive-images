/**
 * @file
 * Attach the ResizeObserver to responsive images to load the best image style.
 */
(function (Drupal) {

  'use strict';

  Drupal.behaviors.easyResponsiveImages = {
    attach: function (context) {
      // helper function to get parent Element which is not inline
      const blockParentNode = function(elementChecked){
        /* OPT : Check if block from display definition.
         const elementBlockDisplays={"run-in":1,"block":1,"table-row-group":1,"table-column":1,"table-column-group":1,"table-header-group":1,"table-footer-group":1,"table-row":1,"table-cell":1,"table-caption":1,"inline-block:":1};
        //var cStyle = $0.currentStyle || window.getComputedStyle($0, "");
        //cStyle.display
        var currentParent = elementChecked.parentElement;
        //while (currentParent && !elementBlockDisplays[getComputedStyle(currentParent).display])
        */

        // Opt 2: Check if parent width is 0 (treat it as inline parent, so
// iterate to next parent.
        var currentParent = elementChecked.parentElement;
        while (currentParent && (currentParent.clientWidth == 0))
          currentParent=currentParent.parentElement;
        return currentParent;
      };

      // Fetch all images containing a "sizes" attribute.
      const images = context.querySelectorAll('img[sizes]');

      // Create a ResizeObserver to update the image "sizes" attribute when its
      // parent container resizes.
      const observer = new ResizeObserver(entries => {
        for (let entry of entries) {
          const images = entry.target.querySelectorAll('img[sizes]');
          images.forEach(image => {
            const parentBlockNode = blockParentNode(image);
            const availableWidth = Math.floor(parentBlockNode.clientWidth);
            image.setAttribute('sizes', availableWidth + "px");
            /*
            If working with vw instead of px:
            const docPixelValue = 100/document.documentElement.clientWidth;
            const containersVW = Math.floor(availableWidth * docPixelValue);

            // Update the "sizes" with the new container width translating to
            // vw
            //image.setAttribute('sizes', containersVW + "vw"); */
          });
        }
      });

      // Attach the ResizeObserver to the image containers.
      images.forEach(image => {
        console.log(image);
        observer.observe(image.parentNode.parentNode);
      });


    }
  };

})(Drupal);
